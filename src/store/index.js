import Vue from "vue";
import Vuex from "vuex";
import router from "@/router";
import accounts from "@/json/accounts.json";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    authenticated: false,
  },

  getters: {
    authenticated: (state) => state.authenticated,
  },

  mutations: {
    SET_LOGGING(state, payload) {
      state.authenticated = payload;
    },
  },

  actions: {
    async logIn({ commit }) {
      commit("SET_LOGGING", true);
    },

    async logOut({ commit }) {
      commit("SET_LOGGING", false);
    },

    async checkCredentials({ dispatch }, { email, password }) {
      if (
        accounts.find((acc) => acc.email === email && acc.password === password)
      ) {
        dispatch("logIn");
        router.push({ path: "/index" });
      } else {
        dispatch("logOut");
      }
    },
  },
});

export default store;
